use std::time::Duration;

/// The maximum rate for any given rate limit.
pub trait Rate {
    /// The maximum amount for the given period.
    fn amount() -> u64;
    /// The period for this maximum rate.
    fn period() -> Duration;
    /// The minimum time between successive messages.
    fn min_delta_time() -> Duration;
}

/// A maximum rate of `N` every `T` seconds.
pub struct PerSecond<const N: u64, const T: u64 = 1>;
/// A maximum rate of `N` every `T` minutes.
pub struct PerMinute<const N: u64, const T: u64 = 1>;
/// A maximum rate of `N` every `T` hours.
pub struct PerHour<const N: u64, const T: u64 = 1>;

impl<const N: u64, const T: u64> Rate for PerSecond<N, T> {
    fn min_delta_time() -> Duration where Self: Sized {
        Duration::from_secs_f64(T as f64 / N as f64)
    }

    fn amount() -> u64 where Self: Sized {
        N
    }

    fn period() -> Duration where Self: Sized {
        Duration::from_secs(T as u64)
    }
}

impl<const N: u64, const T: u64> Rate for PerMinute<N, T> {
    fn min_delta_time() -> Duration where Self: Sized {
        Duration::from_secs_f64(T as f64 / N as f64 * 60.0)
    }

    fn amount() -> u64 where Self: Sized {
        N
    }

    fn period() -> Duration where Self: Sized {
        Duration::from_secs(T as u64 * 60)
    }
}

impl<const N: u64, const T: u64> Rate for PerHour<N, T> {
    fn min_delta_time() -> Duration where Self: Sized {
        Duration::from_secs_f64(T as f64 / N as f64 * 3600.0)
    }

    fn amount() -> u64 where Self: Sized {
        N
    }

    fn period() -> Duration where Self: Sized {
        Duration::from_secs(T as u64 * 3600)
    }
}