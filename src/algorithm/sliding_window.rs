use std::{ marker::PhantomData, time::Duration };

use crate::{ Rate, Algorithm };

#[cfg(test)]
use tests::{ SystemTime, UNIX_EPOCH };
#[cfg(not(test))]
use std::time::{ SystemTime, UNIX_EPOCH };

pub struct SlidingWindow<R: Rate> {
    rate: PhantomData<R>,

    windows: [u64; 2],
    window_start: SystemTime,
}

impl<R: Rate> Algorithm for SlidingWindow<R> {
    fn new(weight: u64) -> Self {
        Self {
            rate: PhantomData,

            windows: [0, weight],
            window_start: SystemTime::now(),
        }
    }

    fn request(&mut self, weight: u64) -> bool {
        let now = SystemTime::now();
        // Assume window 2
        let difference = now.duration_since(self.window_start).unwrap_or_default();

        let ratio = difference.as_secs_f64() / R::period().as_secs_f64();

        if ratio >= 1.0 {
            if ratio >= 2.0 {
                // Blank windows
                self.windows = [0; 2];
            }
            else {
                // Left shift
                self.windows[0] = self.windows[1];
                self.windows[1] = 0;
            }
        }

        let current_window_proportion = ratio.fract();
        let previous_window_proportion = 1.0 - current_window_proportion;

        self.window_start = now - R::period().mul_f64(current_window_proportion);

        let amount = self.windows[0] as f64 * previous_window_proportion + self.windows[1] as f64;

        if amount < R::amount() as f64 {
            self.windows[1] += weight;
            true
        } else {
            false
        }
    }

    fn serialise(self) -> Vec<u8> {
        let timestamp = self.window_start.duration_since(UNIX_EPOCH).unwrap().as_micros() as u64;

        let mut vec = Vec::with_capacity((3 * u64::BITS / 8) as usize);

        vec.extend(self.windows[0].to_le_bytes());
        vec.extend(self.windows[1].to_le_bytes());
        vec.extend(timestamp.to_le_bytes());

        vec
    }
    fn deserialise(value: Vec<u8>) -> Self where Self: Sized {
        let windows = [
            u64::from_le_bytes(value[0..8].try_into().unwrap()),
            u64::from_le_bytes(value[8..16].try_into().unwrap())
        ];

        let timestamp = u64::from_le_bytes(value[16..].try_into().unwrap());

        Self {
            rate: PhantomData,
            windows,
            window_start: UNIX_EPOCH + Duration::from_micros(timestamp),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::PerSecond;

    use std::time::Duration;

    pub const UNIX_EPOCH: SystemTime = SystemTime { time: 0 };

    static mut TIME: u64 = 0;

    #[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
    pub struct SystemTime {
        time: u64,
    }

    impl SystemTime {
        pub fn now() -> Self {
            Self {
                time: unsafe { TIME },
            }
        }

        pub fn duration_since(&self, other: Self) -> Option<Duration> {
            Some(Duration::from_secs(self.time - other.time))
        }
    }

    impl std::ops::Add<Duration> for SystemTime {
        type Output = SystemTime;

        fn add(self, rhs: Duration) -> Self::Output {
            SystemTime {
                time: self.time + rhs.as_secs()
            }
        }
    }

    impl std::ops::Sub<Duration> for SystemTime {
        type Output = SystemTime;

        fn sub(self, rhs: Duration) -> Self::Output {
            SystemTime {
                time: self.time - rhs.as_secs()
            }
        }
    }

    #[test]
    fn sliding_window() {
        let mut algorithm: SlidingWindow<PerSecond<1, 3>> = SlidingWindow::new(1);

        unsafe { TIME += 1; }
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(algorithm.request(1));
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(!algorithm.request(1));
        unsafe { TIME += 1; }
        assert!(algorithm.request(1));
        assert!(!algorithm.request(1));
    }
}