mod sliding_window; pub use sliding_window::*;

pub trait Algorithm {
    fn new(weight: u64) -> Self;

    fn request(&mut self, weight: u64) -> bool;

    fn serialise(self) -> Vec<u8>;
    fn deserialise(value: Vec<u8>) -> Self where Self: Sized;
}